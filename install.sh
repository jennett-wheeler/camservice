#!/bin/bash

if ! sudo -v &> /dev/null; then
    echo "Error: Not sudo. Cannot install service." >&2
    exit 1
fi

dir="$(readlink -f "$(dirname "${BASH_SOURCE}")")"

sudo ln -s ${dir}/streamcam.sh /usr/bin/streamcam
sudo ln -s ${dir}/streamcam.service /etc/systemd/system/

sudo systemctl daemon-reload
sudo systemctl enable streamcam.service
